# Status report

- Groupe : Tuteurs
- Date : 27/06/2022

## Ordre du jour

- [Status report](#status-report)
  - [Ordre du jour](#ordre-du-jour)
  - [Plan d'ensemble](#plan-densemble)
  - [Activités](#activités)
  - [Risques et problèmes](#risques-et-problèmes)
  - [Discussions de contenu](#discussions-de-contenu)

## Plan d'ensemble

![Plan](plans/22-06-27.png)

## Activités

| Activités terminées la semaine passée | Activités planifiées la semaine prochaine | 
| ------ | ------ |
| Création d'un tremplin |  |
| création d'un prototype de véhicule | Amélioration du prototype |
| Rédaction d'une note de synthèse sur le projet | Rédaction d'une 1ère version des documents pour le BAPP |

## Risques et problèmes

| Impact \ Probability | Medium     | High  | Issue |
| --                   | --         | --    | --    |
|                      |            |       |       |
| **High**             |            |       |       |
|                      | Délai de livraison |       |       |
|                      |            |       |       |
| **Medium**           |            |       |       |

## Discussions de contenu

La note de synthèse décrivant le projet est approuvée.  Elle servira de base à la rédaction des documents à rendre au BAPP.  
Le plus urgent est le *descriptif court*.

Le prototype n'est pas capable de sauter avec le tremplin actuel : il perd trop de vitesse dessus.  
Nous obtenons un résultat satisfaisant avec une table inclinée de 10° environ.  
Sur base de ces essais, nous envisageons 2 hypothèses :

* incliner les panneaux composant le circuit au lieu d'utiliser un tremplin.  Cela complique la modélisation du déplacement, mais résout le problème potentiel de puissance des moteurs.
* changer la batterie du prototype pour pouvoir alimenter les moteurs avec une tension plus élevée.

Dans les 2 cas, le choix du projet est validé.

Michel va modifier le prototype à son retour de congés et de nouveaux tests seront effectués le 12/07.  
Laurent va rédiger un 1er jet des documents pour le BAPP.
