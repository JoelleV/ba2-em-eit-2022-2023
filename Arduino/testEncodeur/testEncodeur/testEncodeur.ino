/*
 */

/* On utilise des #define pour pouvoir utiliser un nom qui a du sens pour les pattes.
 *  Cela rend le code plus lisible.
 *  C'est également utile si on doit changer de pattes au cours du projet : on ne doit modifier qu'une ligne de code .
 */

// Pattes de l'encodeur
#define CHA  4
#define CHB  5

#define PERIOD  500

volatile long isrTickCount;
long tickCount;
long t0;


void setup() {
  Serial.begin(9600);

  pinMode(CHA, INPUT_PULLUP);
  pinMode(CHB, INPUT_PULLUP);
  tickCount = 0;
  isrTickCount = 0;
  attachInterrupt(digitalPinToInterrupt(CHA), encoderIsr, CHANGE);

  t0 = millis();
}


void loop() {
  if (millis() >= t0 + PERIOD) {
    t0 = t0 + PERIOD;
    // Section critique
    noInterrupts();
    tickCount = isrTickCount;
    interrupts();
    Serial.println(tickCount);
  }
}


void encoderIsr(void) {
  if (digitalRead(CHA) == digitalRead(CHB)) {
    isrTickCount++;
  } else {
    isrTickCount--;
  }
}
